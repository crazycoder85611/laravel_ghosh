$(document).ready(function(){
	// debugger;
	$('.btn-delete').on('click', function(e){
		if (!confirm('Are you sure?')) e.preventDefault();
	})
})

/*globals $, jQuery, CSPhotoSelector */

$(document).ready(function () {
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});
	var selector, callbackAlbumSelected, callbackPhotoUnselected, callbackSubmit;
	var buttonOK = $('#CSPhotoSelector_buttonOK');
	var o = this;
	var fbUserId, fbUserEmail, fbUserName, fbAccessToken;


	/* --------------------------------------------------------------------
	 * Photo selector functions
	 * ----------------------------------------------------------------- */

	fbphotoSelect = function(id) {
		// if no user/friend id is sent, default to current user
		if (!id) id = 'me';

		callbackAlbumSelected = function(albumId) {
			var album, name;
			album = CSPhotoSelector.getAlbumById(albumId);
			// show album photos
			selector.showPhotoSelector(null, album.id);
		};

		callbackAlbumUnselected = function(albumId) {
			var album, name;
			album = CSPhotoSelector.getAlbumById(albumId);
		};

		callbackPhotoSelected = function(photoId) {
			var photo;
			photo = CSPhotoSelector.getPhotoById(photoId);
			buttonOK.show();
		};

		callbackPhotoUnselected = function(photoId) {
			var photo;
			album = CSPhotoSelector.getPhotoById(photoId);
			buttonOK.hide();
		};

		callbackSubmit = function(photoIds) {
			var photo;
			var photos = {};
			for (photoId in photoIds) {
				photo = CSPhotoSelector.getPhotoById(photoIds[photoId]);
				photos[photoId] = {'photo_id': photo.id, 'photo_source': photo.source};
			}

			if(photos.length !== 0) {
				$.post('ajax/createOrder', { photos: JSON.stringify(photos) }, function(data){
					console.log(data);
				})
			}

		};


		// Initialise the Photo Selector with options that will apply to all instances
		CSPhotoSelector.init({debug: false});

		// Create Photo Selector instances
		selector = CSPhotoSelector.newInstance({
			callbackAlbumSelected	: callbackAlbumSelected,
			callbackAlbumUnselected	: callbackAlbumUnselected,
			callbackPhotoSelected	: callbackPhotoSelected,
			callbackPhotoUnselected	: callbackPhotoUnselected,
			callbackSubmit			: callbackSubmit,
			maxSelection			: 10,
			albumsPerPage			: 6,
			photosPerPage			: 200,
			autoDeselection			: true
		});

		// reset and show album selector
		selector.reset();
		selector.showAlbumSelector(id);
	}


	/* --------------------------------------------------------------------
	 * Click events
	 * ----------------------------------------------------------------- */

	$("#btnLogin").click(function (e) {
		e.preventDefault();
/*		$.ajax({
            type:"GET",
		    url:'ajax_login',
            data:{'fbuid':fbAppId,'email':fbUserEmail,'name': fbUserName,'access_token':fbAccessToken },
            success: function(data){
            	alert(data);
            	window.location.assign(APP_URL+"/clientDashboard");
    		}
		});*/
		FB.login(function (response) {
			if (response.authResponse) {
				FB.api('/me?fields=email,name', function(response) {
				  	fbUserId 		= response.id;
				  	fbUserEmail 	= response.email;
				  	fbUserName 		= response.name;
				  	fbAccessToken 	= FB.getAuthResponse()['accessToken'];

					$.ajax({
			            type:"GET",
					    url:'ajax_login',
			            data:{'fbuid':fbUserId,'email':fbUserEmail,'name': fbUserName,'access_token':fbAccessToken },
			            success: function(data){
			            	window.location.assign(APP_URL+"/clientDashboard");
			    		}
					});
				});
				$("#login-status").html("Logged in");
			} 
			else 
			{
				$("#login-status").html("Not logged in");
			}
		}, {
			scope: 'email,user_photos',
			return_scopes: true
		});
	});

	$("#btnLogout").click(function (e) {
		e.preventDefault();
		FB.logout();
		$("#login-status").html("Not logged in");
	});

	$(".photoSelect").click(function (e) {
		e.preventDefault();
		fbphotoSelect();
	});
})


window.fbAsyncInit = function() {
    FB.init({
      appId      : fbAppId,
      xfbml      : true,
      version    : 'v2.9'
    });
    FB.AppEvents.logPageView();

	// FB.getLoginStatus(function(response) {
	// 	if (response.authResponse) {
	// 		$("#login-status").html("Logged in");
	// 	} else {
	// 		$("#login-status").html("Not logged in");
	// 	}	
	// });
};

(function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));