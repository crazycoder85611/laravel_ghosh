<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::get('/', 'Home@index');
Route::get('clientDashboard', 'Home@clientDashboard');
Route::get('clientOrders', 'Home@clientOrders');
Route::get('adminDashboard', 'Home@adminDashboard');
Route::get('adminOrders', 'Home@adminOrders');
Route::get('adminCustomers', 'Home@adminCustomers');
Route::get('adminOrdersImages/{param}', 'Home@Images');
Route::get('adminOrderscreateOrder/{param}', 'Home@createOrder');
Route::get('adminOrdersImages/deleteImage/{param}', 'Home@deleteImage');
Route::get('ajax_login', 'Ajax@login');

