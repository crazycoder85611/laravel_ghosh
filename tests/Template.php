<?php 
namespace Tests;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

class Template {
	private $mTitle;
	private $mDescription;

	/**
     * Set meta title
     * @author Mamikon Kanoyan
     * @return string
     */
	public function setTitle($value) {
		$this->mTitle = $value;
	}

	/**
     * Get meta title
     * @author Mamikon Kanoyan
     * @return string
     */
	public function getTitle() {
		return $this->mTitle;
	}

	/**
     * Set meta description
     * @author Mamikon Kanoyan
     * @return string
     */
	public function setDescription($value) {
		$this->mDescription = $value;
	}

	/**
     * Get meta description
     * @author Mamikon Kanoyan
     * @return string
     */
	public function getDescription() {
		return $this->mDescription;
	}

	/**
     * Load theme js
     * @author Mamikon Kanoyan
     * @return string
     */
	public function loadJs($scripts = array()) {
		$html = '';
		foreach ($scripts as $script) {
			$html .= '<script type="text/javascript" src="'.base_url(MY_ASSETS_LOCATION.DIRECTORY_SEPARATOR.$script).'" ></script>'."\n\r";
		}
 		return $html;
	}

	/**
     * Load theme css
     * @author Mamikon Kanoyan
     * @return string
     */
	public function loadCss($styles = array()) {
		$html = '';
		foreach ($styles as $style) {
			$html .= '<link rel="stylesheet" type="text/css" href="'.base_url(MY_ASSETS_LOCATION.DIRECTORY_SEPARATOR.$style).'">'."\n\r";
		}
 		return $html;
	}
}