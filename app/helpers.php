<?php
// namespace App\helpers;
// use Illuminate\Cookie\CookieJar;

 function get_facebook_cookie() {
    $app_id =config('config.facebook_app_id');
    $application_secret =config('config.facebook_app_secret');
    if(isset($_COOKIE['fbsr_' . $app_id])){
        list($encoded_sig, $payload) = explode('.', $_COOKIE['fbsr_' . $app_id], 2);

        $sig = base64_decode(strtr($encoded_sig, '-_', '+/'));
        $data = json_decode(base64_decode(strtr($payload, '-_', '+/')), true);

        if (strtoupper($data['algorithm']) !== 'HMAC-SHA256') {
            return null;
        }
        $expected_sig = hash_hmac('sha256', $payload,
            $application_secret, $raw = true);
        if ($sig !== $expected_sig) {
            return null;
        }
        $token_url = "https://graph.facebook.com/oauth/access_token?"
        . "client_id=" . $app_id . "&client_secret=" . $application_secret. "&redirect_uri=" . "&code=" . $data['code'];

        $response = @file_get_contents($token_url);
        $params = null;
        parse_str($response, $params);
        if(!empty($params['access_token'])) {
            $data['access_token'] = $params['access_token'];
        }
        return $data;
    }else{
        return null;
    }
}
?>