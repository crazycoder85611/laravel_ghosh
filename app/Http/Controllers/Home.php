<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Routing\UrlGenerator;
use Session;
use App\Http\Controllers\PhpPwinty;

class Home extends BaseController
{
	//----------Variable difinition-----------
	public	$table = "orders";
	public  $records = array();
	public  $fb = array();
	public  $admins = array();
	private  $is_admin = false;	
	private  $images_count=0;
	private  $url;
	public function __construct(UrlGenerator $url) {
		// parent::__construct();
		$results = array(
			'email'=>'sss@gmail.com',
			'id'=>'dsf'
		   );		
		$this->fb = get_facebook_cookie();
		if(empty($fb)) {
			// echo ('$fb:'.$fb);			
		}
		else {
			$usr_id=$this->fb['user_id'];
			$results = DB::select( DB::raw("SELECT * FROM customer WHERE fbuid='$usr_id'"));
		}

		$admins =config('config.facebook_admins');
			// echo ('$admins:'.$admins[0]);			
		if(in_array($results['email'], $admins)) {
			$this->is_admin=true;
		}
	}

	public function index()
	{
		return view('home',['title'=>'Home','description'=>'Home description']);
	}

	public function clientOrders()
	{
				
		$this->records = DB::table('orders')->where('fbuid', '=', $this->fb['user_id'])->paginate(15);
		$this->images_count= DB::table('order_items')->count();
		return view('clientOrders',['title'=>'Orders','description'=>'Orders description','isAdmin'=>$this->is_admin, 'records'=>$this->records,'images_count'=>$this->images_count]);
	}


	public function clientDashboard()
	{
		return view('clientDashboard',['title'=>'clientDashboard','description'=>'clientDashboard description','isAdmin'=>$this->is_admin]);
	}

	public function adminDashboard()
	{
		return view('adminDashboard',['title'=>'adminDashboard','description'=>'adminDashboard description','isAdmin' => $this->is_admin]);
	}
	public function adminOrders()
	{
		$images[]=array('images_count'=>'');
		$users = DB::table('order_items')
				->select('order_id',DB::raw('count(order_items.order_id) as images_count'))
				->groupBy('order_id')
				->get();
		$i=0;
		$j=[];

		foreach ($users as $value) {
			if(intval($value->order_id)==$i)
			{
				$images[$i]['images_count']=$value->images_count;
	 			$i++;
			}
			else
			{
				do {
					$images[$i]['images_count']=1;					
				    $i++;
				   	if(intval($value->order_id)==$i)
				   	{
						$images[$i]['images_count']=$value->images_count;
				    	$i++;
				    	break;
				   	}
				} while ($i <= count($users));				
			}
 			$j[$i]=$value->order_id; 
		}
		$this->records = DB::table('orders')
				->select('orders.*')
				->orderBy('orders.id')
				->paginate(15);
		$crr_pg=$this->records->currentPage();
		return view('adminOrders',['title'=>'adminOrders','description'=>'adminOrders description','isAdmin'=>$this->is_admin,'records'=>$this->records,'images_count'=>$images,'crrent_pg'=>$crr_pg,'img_cnt'=>$j]);
	}
	public function adminCustomers()
	{
		$this->records = DB::table('customer')->paginate(15);
		return view('adminCustomers',['title'=>'Customers','description'=>'Customers description','isAdmin'=>$this->is_admin,'records'=>$this->records]);
	}

    public function Images($id = null) {
		$this->records = DB::table('order_items')
				->select('*')
				->where('order_id', '=', $id)
				->paginate(10);
		return view('adminOrdersImages',['title'=>'adminOrdersImages','description'=>'Orders Images description','isAdmin'=>$this->is_admin,'records'=>$this->records]);
    }

    public function deleteImage($id = null) {
    	if($id !== null)
    	{
			DB::table('order_items')->where('id', '=', $id)->delete();        
    	}
		$this->records = DB::table('order_items')
				->select('*')
				->where('order_id', '=', $id)
				->paginate(10);
		return view('adminOrdersImages',['title'=>'adminOrdersImages','description'=>'Orders Images description','isAdmin'=>$this->is_admin,'records'=>$this->records]);
    }

	public function createOrder($id) {

/*        $options = array(
            'api'        => 'sandbox',
            'merchantId' => '8d19807d-8670-4efd-96da-e7a5cb0a728e',
            'apiKey'     => 'e340f335-30e0-4482-9abe-221bb7ec74bd'
        );
        $PhpPwintyclass=new PhpPwinty($options);        
		$orders = DB::table('orders')
				->select('*')
				->where('id', '=', $id)
				->get();        
		$customer = DB::table('customer')
				->select('*')
				// ->where('fbuid', '=', $orders->fbuid)
				->get();        

        $order = $PhpPwintyclass->createOrder(
            $customer->name, //name
            $customer->email, //email
            $customer->address1, //address1
            $customer->address2, //address 2
            $customer->town, //town
            $customer->state, //state
            $customer->postcode, //postcode or zip
            $customer->country_code, //country code
            $customer->country_code, //destination code
            true, //tracked shipping
            "InvoiceMe", //payment method
            "Pro" //quality
        );

        if(empty($order['errorMessage']) && !empty($order['id'])) {

			$images = DB::table('order_items')
				->select('*')
				->where('order_id', '=', $id)
				->get();        

            foreach($images as $image) {
                if(!empty($image->photo_src)) {
                    $picture_format = null;
                    list($width, $height) = getimagesize($image->photo_src);
                    $size = getimagesize($image->photo_src);
                    if($width > $height){
                        echo $picture_format = ($width / $height < 1.3 || $width / $height > 1.7) ?  "ShrinkToFit" : "ShrinkToExactFit";
                    }else{
                        echo $picture_format = ($height / $width < 1.3 || $height / $width > 1.7) ?  "ShrinkToFit" : "ShrinkToExactFit";
                    }
                    if( ! $picture_format ){ $picture_format = "ShrinkToFit"; }

                    $PhpPwintyclass->addPhoto(
                        $order['id'], //order id
                        "4x6", //print size
                        $image->photo_src, //image url
                        "1", //print quantity
                        $picture_format, //resize method
                        "2000" //price to user
                    );
                }
            }

			DB::table('users')->insert(
			    ['fbuid' => $customer->fbuid, 'order_id' => $id,'order_id_api'=>$order['id'],'price'=>$order['price'],'payment_type'  => $order['payment'],'payment_url'   => $order['paymentUrl'],'status'        => $order['status'],'type'          => 'pwinty']
			);
			DB::table('orders')
            	->where('id', $id)
            	->update(['status' => 'sent']);

            $result = $PhpPwintyclass->updateOrderStatus($order['id'], "Submitted");
        }
        Session::put('flashSuccess','Order successfully sent!');*/
        
        return redirect('adminOrders');
    }

}

