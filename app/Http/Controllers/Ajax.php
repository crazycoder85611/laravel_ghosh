<?php
namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
// use Illuminate\Contracts\Validation\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;
use Illuminate\Contracts\Auth\Registrar as RegistrarContrac;
use Session;

class Ajax extends BaseController {

	private $response = array();
	private $success = array();
	private $error = array();

	/**
     * Class constructor
     * @author Mamikon Kanoyan
     * @return void
     */
	public function __construct() {
		// parent::__construct();
	}

	/**
     * Run after FB login
     * @author Mamikon Kanoyan
     * @return json
     */
	public function login()	{
/*		$fbuid = Input::get("fbuid");
		if (Session::has('fbuid_back')) {
		  	echo Session::get('fbuid_back');
		}		
		else
		{
			Session::put('fbuid_back', $fbuid);
		  	echo 'No data saved previously';
		}*/
		$fbuid = Input::get("fbuid");
		$email = Input::get("email");
		$name = Input::get("name");
		$access_token = Input::get("access_token");
		$validator = Validator::make(
			array(
	            'Facebook ID' => $fbuid,
	            'Email' => $email,
	            'Name'  => $name,
	            'Access Token' =>$access_token
        	),
			array(
	            'Facebook ID' => 'required|numeric',
	            'Email' => 'required|email',
	            'Name'  => 'required',
	            'Access Token' =>'required'
        	)
		);		
		if ($validator->fails())
		{
		    // The given data did not pass validation
		    $messages = $validator->messages();
		    echo $messages;
		}
		else
		{
			$results = DB::select( DB::raw("SELECT * FROM customer WHERE fbuid='$fbuid'"));
            $date=date('Y-m-d H:i:s');             	
            if(!$results) 
            {
				$results = DB::select( DB::raw("INSERT INTO customer (fbuid, access_token, name, email, created_at)	VALUES ('$fbuid', '$access_token', '$name', '$email', '$date')"));
				// echo ("insert");
            }
            else
            {
    			$results = DB::select( DB::raw("UPDATE customer SET fbuid='$fbuid', access_token='$access_token', name='$name', email='$email', created_at='$date' WHERE fbuid='$fbuid'"));
				// echo ("update");
            }
			echo ("success");
		}
	}

	/**
     * Create order using fb pics
     * @author Mamikon Kanoyan
     * @return json
     */
	public function createOrder() {
		$photos = $this->input->post('photos', true);
		out($this->user);
		if( !empty($photos) ) {
			$photos = json_decode($photos, true);
			if(is_array($photos)) {
				foreach ($photos as $photo) {
					out($photo);
				}
			}
		}
	}

	/**
     * Ajax response
     * @author Mamikon Kanoyan
     * @return json
     */
	private function response() {

		if(!empty($this->error)) {
			$this->response['type'] = 'error';
			$this->response['errors'] = $this->error;
		} elseif (!empty($this->success)) {
			$this->response['type'] = 'success';
			$this->response['response'] = $this->success;
		}

		$this->output
	        ->set_content_type('application/json')
	        ->set_output(json_encode($this->response));
	}

}
