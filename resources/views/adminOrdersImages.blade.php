@extends('master')
@section('header')
    @include('adminHeader')
@endsection
@section('content')
<div class="container">
    <div class="row">
        @if(Session::has('success'))
            <?php $success = json_decode(Session::get('success')); ?>
            <div class="alert alert-success" role="alert">
                <?php out($success); ?>
            </div>
        @endif
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <?php echo $title; ?>list
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body panel-list">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Object ID</th>
                                    <th>Images</th>
                                    <th class="records_actions"></th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($records as $key => $record)
                                <tr>
                                    <td>{{ $record->id }}</td>
                                    <td>{{ $record->object_id }}</td>
                                    <td><a href="{{ $record->photo_src }}" target="_blank"><img src="{{ $record->photo_src }}" height="60"></a></td>
                                    <td><a href="{{ url('/') }}'/adminOrdersImages/deleteImage/'.{{ $record->id }}" class="btn btn-xs btn-danger btn-delete"><i class="glyphicon glyphicon-trash"></i></a></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.panel-body -->
                <div class="panel-footer">
                    <nav class="text-center">
                        <?php echo $records->links(); ?>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('footer')
    <div class="container">
        <p class="text-muted">Place sticky footer content here.</p>
    </div>
@endsection
