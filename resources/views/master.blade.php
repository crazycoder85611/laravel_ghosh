<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $title; ?></title>
    <meta name="description" value="<?php echo $description; ?>" >
    <link href="{{ asset('/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <script type="text/javascript">
        var fbAppId = "{{ config('config.facebook_app_id')}}";
    </script>
</head>
<body>
    <header role="banner">
        @yield('header') 
    </header>
    <div class="main-content" >
        @yield('content')
    </div>

    <footer class="footer color-bg">
        @yield('footer')
        <script type="text/javascript">
            var APP_URL = {!! json_encode(url('/')) !!};
        </script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>  
        <script src="{{ asset('/js/bootstrap.min.js') }}"></script>    
        <script src="{{ asset('/js/csphotoselector.js') }}"></script>    
        <script src="{{ asset('/js/scripts.js') }}"></script>    
    </footer>
</body>
</html>
