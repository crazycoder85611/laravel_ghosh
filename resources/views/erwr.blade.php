<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8"><meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $this->template->getTitle(); ?></title>
    <meta name="description" value="<?php echo $this->template->getDescription(); ?>" >
    <?php echo $this->load->template->loadCss(array(
        'css/bootstrap.min.css',
        'css/style.css'
    ));?>
    <script type="text/javascript">
        var fbAppId = "<?=$this->config->item('facebook_app_id');?>";
    </script>
</head>
<body>
    <nav class="navbar navbar-default">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">Admin</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li class="active">
                        <a href="<?php echo base_url('admin/dashboard'); ?>">Dashboard</a>
                    </li>
                    <li>
                        <a href="<?php echo base_url('admin/orders'); ?>">Orders</a>
                    </li>
                    <li>
                        <a href="<?php echo base_url('admin/customers'); ?>">Customers</a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Settings <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="#">General</a></li>
                            <li><a href="#">Pwinty</a></li>
                        </ul>
                    </li>
                </ul>
                <?php if($this->auth->isAdmin()): ?>
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="<?php echo base_url('dashboard'); ?>">Client</a>
                    </li>
                </ul>
                <?php endif; ?>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>

    <?php if( $this->session->flashdata('flashSuccess') || $this->session->flashdata('flashError') ): ?>
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <?php if( $this->session->flashdata('flashSuccess') ): ?>
                        <div class="alert alert-success" role="alert"><?=$this->session->flashdata('flashSuccess')?></div>
                    <?php endif; ?>
                    <?php if( $this->session->flashdata('flashError') ): ?>
                        <div class="alert alert-danger" role="alert"><?=$this->session->flashdata('flashError')?></div>
                    <?php endif; ?>
                </div>
            </div>
            </div>
    <?php endif; ?>