@extends('master')
@section('header')
    {{-- @include('eventStreamsTemplate',['streams'=>$allStreams]) --}}
    @include('adminHeader')
@endsection
@section('content')
<div class="container" style="padding: 0px; width: 90%;" >
    <div class="row">
        <div class="col-lg-12" style="padding: 0px">
            <div class="btn-group pull-right" role="group" style="padding: 0px">
                <button type="button" class="btn btn-default" data-toggle="modal" data-target="#updateTokens">Update Access Tokens</button>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12" style="padding: 0px">
            <div class="panel panel-default" style="padding: 0px">
                <div class="panel-heading">
                    <?php echo $title; ?> list
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body panel-list">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>fbuid</th>
                                    <th>name</th>
                                    <th>email</th>
                                    <th>address1</th>
                                    <th>address2</th>
                                    <th>town</th>
                                    <th>state</th>
                                    <th>postcode</th>
                                    <th>country_code</th>
                                    <th>status</th>
                                    <th>created_at</th>
                                    <th>updated_at</th>
                                    <th class="records_actions"></th>
                              </tr>
                            </thead>
                            <tbody>
                            @foreach($records as $key => $record)
                                <tr>
                                    <td>{{ $record->id }}</td>
                                    <td>{{ $record->fbuid }}</td>
                                    <td>{{ $record->name }}</td>
                                    <td>{{ $record->email }}</td>
                                    <td>{{ $record->address1 }}</td>
                                    <td>{{ $record->address2 }}</td>
                                    <td>{{ $record->town }}</td>
                                    <td>{{ $record->state }}</td>
                                    <td>{{ $record->postcode }}</td>
                                    <td>{{ $record->country_code }}</td>
                                    <td>{{ $record->status }}</td>
                                    <td>{{ $record->created_at }}</td>
                                    <td>{{ $record->updated_at }}</td>
                                </tr>
                            @endforeach
                           </tbody>
                        </table>
                    </div>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.panel-body -->
                <div class="panel-footer">
                    <nav class="text-center">
                        <?php echo $records->links(); ?>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Update Access Tokens -->
<div class="modal fade" id="updateTokens" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="#" method="post" enctype="multipart/form-data">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Update Tokens</h4>
                </div>
                <div class="modal-body">
                    <h5>CSV example:</h5>
                    <p>
                        "fbuid","access_token"<br>
                        {fbuid},{access_token}<br>
                        {fbuid},{access_token}<br>
                        {fbuid},{access_token}<br>
                    </p>
                    <input type="file" name="csv"></input>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Import</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Import Orders Photos -->
<div class="modal fade" id="importOrdersPhotos" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="#" method="post" enctype="multipart/form-data">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Import Orders Photos</h4>
                </div>
                <div class="modal-body">
                        <input type="file" name="csv"></input>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Import</button>
                </div>
            </form>
        </div>
    </div>
</div>
</div>
@endsection
@section('footer')
    <div class="container">
        <p class="text-muted">Place sticky footer content here.</p>
    </div>
@endsection

