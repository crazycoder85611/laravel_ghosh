
<nav class="navbar navbar-default">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="adminDashboard">Admin</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="active">
                    <a href="{{ url('/') }}/adminDashboard">Dashboard</a>
                </li>
                <li>
                    <a href="{{ url('/') }}/adminOrders">Orders</a>
                </li>
                <li>
                    <a href="{{ url('/') }}/adminCustomers">Customers</a>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Settings <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="#">General</a></li>
                        <li><a href="#">Pwinty</a></li>
                    </ul>
                </li>
            </ul>
            @if($isAdmin)
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="clientDashboard">Client</a>
                    </li>
                </ul>

            @endif
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>
@if(Session::has('flashSuccess') || Session::has('flashError'))
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                @if(Session::has('flashSuccess'))
                    <div class="alert alert-success" role="alert">
                        @php Session::get('flashSuccess') @endphp
                    </div>
                @elseif(Session::has('flashError'))
                    <div class="alert alert-danger" role="alert">
                        @php Session::get('flashError') @endphp
                    </div>
                @endif
            </div>
        </div>
    </div>
@endif
