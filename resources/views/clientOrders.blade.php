@extends('master')
@section('header')
    @include('clientHeader')
@endsection
@section('content')
<div class="container">
    <div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class="btn-group pull-right" role="group">
                <a href="#" class="btn btn-default">Grab New Orders FB pics</a>
                <a href="#" class="btn btn-default">Grab All FB pics</a>
                <button type="button" class="btn btn-default" data-toggle="modal" data-target="#importOrders">Import Orders</button>
                <button type="button" class="btn btn-default" data-toggle="modal" data-target="#importOrdersPhotos">Import Orders Photos</button>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <?php echo $title; ?> list
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body panel-list">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Images</th>
                                    <th>Created at</th>
                                    <th>Status</th>
                                    <th class="records_actions"></th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($records as $key => $record)
                                <tr>
                                    <td> {{ $record->id }}</td>
                                    <td><a href="{{ 'https://www.facebook.com/'.$record->fbuid }}"> {{ $record->name }}</a></td>
                                    <td>{{ $record->email }}</td>
                                    <td>{{ $images_count }}</td>
                                    <td>{{ $record->created_at }}</td>
                                    <td>{{ $record->status }}</td>
                               </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.panel-body -->
                <div class="panel-footer">
                    <nav class="text-center">
                        <?php echo $records->links(); ?>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Import Orders -->
<div class="modal fade" id="importOrders" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="#" method="post" enctype="multipart/form-data">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Import Orders</h4>
                </div>
                <div class="modal-body">
                        <input type="file" name="csv"></input>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Import</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Import Orders Photos -->
<div class="modal fade" id="importOrdersPhotos" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="#" method="post" enctype="multipart/form-data">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Import Orders Photos</h4>
                </div>
                <div class="modal-body">
                        <input type="file" name="csv"></input>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Import</button>
                </div>
            </form>
        </div>
    </div>
</div>
</div>
@endsection
@section('footer')
    <div class="container">
        <p class="text-muted">Place sticky footer content here.</p>
    </div>
@endsection

